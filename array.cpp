/* Author: Marek Przygocki
 * date: 24.11.2019
 * array, range loops, exceptions, iterator, lambda function, sorting*/

#include <iostream>
#include <array>
#include <string>
#include <algorithm>

using namespace std;

array<string,5> lista{"papier","maslo","atlas","monitor","jogurt"};

int main()
{
    for (int i=0; i<5; i++)
        cout << lista[i] << endl;
    cout << endl;

    for (int i=0; i<5; i++)
        cout << lista.at(i) << endl;
    cout << endl;

    for (const auto i : lista)
        cout << i << endl;
    cout << endl;

    cout << "rozmiar tablicy: " << lista.size() << endl;
    cout << endl;

    cout << boolalpha;
    cout << "tablica jest pusta: " << lista.empty();
    cout << endl;

    //zamiana elementow na litere a
    for (auto& i : lista) {
        if (i[0] == 'a')
            i = "zamienione";
    }
    cout << endl;
    
    for (const auto i : lista)
        cout << i << endl;
    cout << endl;

    //wypisanie iteratorem wstecz
    for (auto it=lista.crbegin(); it != lista.crend(); ++it) {
        std::cout << *it << endl;
    }
	cout << endl;
	
    try {
        cout << lista.at(10);
    } catch (const out_of_range& e) {
        cout << "nasz wyjątek:" << e.what();
    }
    cout << endl << endl;

    //sortowanie rosnaco
    sort(lista.begin(), lista.end());

    cout << "lista po sortowaniu rosnaco: " << endl;
    for (const auto i : lista)
        cout << i << endl;
    cout << endl;

    //sortowanie listy w kolejności rosnącej przy użyciu własnej funkcji porównawczej
    sort(lista.begin(), lista.end(),[](const string& a, const string& b){return (a<b);});

    cout << "lista po sortowaniu rosnaco wg wlasnej funkcji: " << endl;
    for (const auto i : lista)
        cout << i << endl;
    cout << endl;

    //sortowanie listy w kolejności rosnącej przy użyciu własnej funkcji porównawczej
    sort(lista.begin(), lista.end(),[](const string& a, const string& b){return (a>b);});

    cout << "lista po sortowaniu malejaco wg wlasnej funkcji: " << endl;
    for (const auto i : lista)
        cout << i << endl;


    return 0;
}

