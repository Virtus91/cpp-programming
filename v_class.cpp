/* Author: Marek Przygocki
 * date: 24.11.2019
 * array, vector, class, operator<<, range loops, lambda function, sorting*/

#include <iostream>
#include <cstdlib>
#include <ctime>
#include <array>
#include <string>
#include <algorithm>
#include <vector>

std::array<std::string,10> names{"Andrzej" ,"Piotr", "Olaf", "Kornel", "Aleksander", "Paweł", "Krzysztof", "Marek", "Kamil", "Artur"};
std::array<std::string,10> surnames{"Nowak" ,"Jagodzinski", "Gadzala", "Kijak", "Slon", "Zlotnicki", "Krzysztofiak", "Urban", "Wojewodzki", "Pogodny"};

class Person {
    std::string name;
    std::string surname;
    int age;

public:
    std::string get_name(){return name;};
	std::string get_surname(){return surname;};
	int get_age() {return age;};
	Person(std::string n,std::string s,int a);
    Person() = default;
    friend std::ostream& operator<<(std::ostream& s, const Person& p);
};

Person::Person(std::string n, std::string s,int a): name(n), surname(s), age(a){}

//operator<<
std::ostream& operator<<(std::ostream& s,const Person& p){
    s << "Imie: " << p.name <<",  Nazwisko: " << p.surname << ", Wiek: " << p.age << std::endl;
	return s;
}

std::vector<Person>t;

void t_content(){
	 for (auto const person : t)
        std::cout << person;
}

int main()
{   
	srand(time(nullptr));

    for (int i=0; i<10; i++){
        t.push_back(Person(names[rand()%10], surnames[rand()%10], rand()%100));
    }
    t_content();

	//sortowanie rosnąco względem imienia.
    std::sort(t.begin(), t.end(),[](Person& a, Person& b)
        {
            return (a.get_name() < b.get_name());
        });

    std::cout << std::endl;
    std::cout << "Po sortowaniu rosnaco wzgledem imienia: " << std::endl;
	t_content();
	
	//sortowanie rosnąco względem nazwiska.
    std::sort(t.begin(), t.end(),[](Person& a, Person& b)
        {
            return (a.get_surname() < b.get_surname());
        });

    std::cout << std::endl;
    std::cout << "Po sortowaniu rosnaco wzgledem nazwiska: " << std::endl;
    t_content();

	//sortowanie rosnąco względem wieku.
    std::sort(t.begin(), t.end(),[](Person& a, Person& b)
        {
            return (a.get_age() < b.get_age());
        });

    std::cout << std::endl;
    std::cout << "Po sortowaniu rosnaco wzgledem wieku: " << std::endl;
    t_content();

	//sortowanie malejaco względem imienia.
    std::sort(t.begin(), t.end(),[](Person& a, Person& b)
        {
            return (a.get_name() > b.get_name());
        });

    std::cout << std::endl;
    std::cout << "Po sortowaniu malejaco wzgledem imienia: " << std::endl;
    t_content();

	//sortowanie malejaco względem nazwiska.
    std::sort(t.begin(), t.end(),[](Person& a, Person& b)
        {
            return (a.get_surname() > b.get_surname());
        });

    std::cout << std::endl;
    std::cout << "Po sortowaniu malejaco wzgledem nazwiska: " << std::endl;
    t_content();
 
	//sortowanie malejaco względem wieku.
    std::sort(t.begin(), t.end(),[](Person& a, Person& b)
        {
            return (a.get_age() > b.get_age());
        });

    std::cout << std::endl;
    std::cout << "Po sortowaniu malejaco wzgledem wieku: " << std::endl;
    t_content();

    return 0;
}
